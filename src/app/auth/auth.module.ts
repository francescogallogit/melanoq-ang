import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {Routes, RouterModule} from "@angular/router";

import {AuthComponent} from './auth.component';
import {ReactiveFormsModule} from "@angular/forms";

import {CookieService} from 'ngx-cookie-service';
import {MainModule} from "../main/main.module";


const routes: Routes = [
  {path: 'auth', component: AuthComponent}
];


@NgModule({
  declarations: [
    AuthComponent
  ],
  exports: [
    RouterModule
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        MainModule
    ],
  providers: [
    CookieService
  ]
})
export class AuthModule {
}
