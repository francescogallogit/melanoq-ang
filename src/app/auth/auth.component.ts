import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { ApiService} from "../api.service";

import { CookieService } from 'ngx-cookie-service';

import { Router } from "@angular/router";

interface TokenObj {
  id: string;
  token: string;
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {


  authForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  }) ;

  parentUser_name = '';

  registerMode = false;

  constructor(
    private apiService: ApiService,
    private cookieService: CookieService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const pToken = this.cookieService.get('patient-token');
    if(pToken) {
      this.router.navigate(['/patients']);
    }
  }

  saveForm() {
    this.apiService.loginUser(this.authForm.value).subscribe(

      // @ts-ignore
      (result: TokenObj) => {
        this.parentUser_name = this.authForm.value.username;
        //console.log("Qui:", result);
        this.cookieService.set('patient-token', result.token);
        this.cookieService.set('patient-id', result.id);
        this.cookieService.set('username', this.parentUser_name);
        this.router.navigate(['/patients']);
        console.log('User:', result.id);
      },
      error => console.log(error)
    );


    console.log('Username:', this.parentUser_name);


  }

}
