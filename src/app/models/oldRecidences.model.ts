export class oldRecidences {
  countryResidency: string;
  cityResidency: string;
  dateFrom: string;
  dateTo: string;

  countryResidency1: string;
  cityResidency1: string;
  dateFrom1: string;
  dateTo1: string;

  countryResidency2: string;
  cityResidency2: string;
  dateFrom2: string;
  dateTo2: string;

  countryResidency3: string;
  cityResidency3: string;
  dateFrom3: string;
  dateTo3: string;

  constructor(countryResidency: string, cityResidency: string, dateFrom: string, dateTo: string,
              countryResidency1: string, cityResidency1: string, dateFrom1: string, dateTo1: string,
              countryResidency2: string, cityResidency2: string, dateFrom2: string, dateTo2: string,
              countryResidency3: string, cityResidency3: string, dateFrom3: string, dateTo3: string) {
    this.countryResidency = countryResidency;
    this.cityResidency = cityResidency;
    this.dateFrom =  dateFrom;
    this.dateTo = dateTo;
    this.countryResidency1 = countryResidency1;
    this.cityResidency1 = cityResidency1;
    this.dateFrom1 =  dateFrom1;
    this.dateTo1 = dateTo1;
    this.countryResidency2 = countryResidency2;
    this.cityResidency2 = cityResidency2;
    this.dateFrom2 =  dateFrom2;
    this.dateTo2 = dateTo2;
    this.countryResidency3 = countryResidency3;
    this.cityResidency3 = cityResidency3;
    this.dateFrom3 =  dateFrom3;
    this.dateTo3 = dateTo3;
  }
}
