export class oldOccupations {
  pastOccupationO: string;
  dateFromPO: string;
  dateToPO: string;

  pastOccupationO1: string;
  dateFromPO1: string;
  dateToPO1: string;

  pastOccupationO2: string;
  dateFromPO2: string;
  dateToPO2: string;

  pastOccupationO3: string;
  dateFromPO3: string;
  dateToPO3: string;


  constructor(pastOccupationO: string, dateFromPO: string, dateToPO: string,
              pastOccupationO1: string, dateFromPO1: string, dateToPO1: string,
              pastOccupationO2: string, dateFromPO2: string, dateToPO2: string,
              pastOccupationO3: string, dateFromPO3: string, dateToPO3: string) {
    this.pastOccupationO = pastOccupationO;
    this.dateFromPO = dateFromPO;
    this.dateToPO =  dateToPO;
    this.pastOccupationO1 = pastOccupationO1;
    this.dateFromPO1 = dateFromPO1;
    this.dateToPO1 =  dateToPO1;
    this.pastOccupationO2 = pastOccupationO2;
    this.dateFromPO2 = dateFromPO2;
    this.dateToPO2 =  dateToPO2;
    this.pastOccupationO3 = pastOccupationO3;
    this.dateFromPO3 = dateFromPO3;
    this.dateToPO3 =  dateToPO3;
  }
}
