export class SectionC {
  solarLentigines: string;
  solarLentiginesAtSite: string;
  neviCount: string;
  neviScalp: number;
  neviThoraxAbdAnt: number;
  neviThoraxAbdBack: number;
  neviFaceLeft: number;
  neviFaceRight: number;
  neviNeckLeft: number;
  neviNeckRight: number;
  neviUpperExtremLeft: number;
  neviUpperExtremRight: number;
  neviLowerExtremLeft: number;
  neviLowerExtremRight: number;
  neviPalmsLeft: number;
  neviPalmsRight: number;
  neviSolesLeft: number;
  neviSolesRight: number;
  neviAtypical: string;
  neviCNMedium: string;
  neviCNMediumSites: string;
  neviCNLarge: string;
  neviCNLargeSites: string;
  neviCNGiant: string;
  neviCNGiantSites: string;
  neviBlue: string;
  neviBlueCount: number;
  neviActinic: string;
  neviActinicSite: string;
  neviActinicType: string;
  neviKSC: string;
  neviKSCCount: number;
  neviKSCSites: string;
  neviBCC: string;
  neviBCCCount: number;
  neviBCCSites: string;
  neviSCC: string;
  neviSCCCount: number;
  neviSCCSites: string;
  HistoryNoCancer: string;
  PreviousTreatments: string;

  PregnancyNumber: number;
  YearOfBirthChildren: number;
  MiscarriagesNumber: number;
  melanomaDuringPregnancy: string;
  melanomaBeforePregnancy: string;
  melanomaBeforePregnancyY: number;
  melanomaAfterPregnancy: string;
  melanomaAfterPregnancyY: number;
  hormonBefore: string;
  BCCount: number;
  BCC: string;
  BCCSites: string;
  BCCDate: Date;
  SCCount: number;
  SCC: string;
  SCCSites: string;
  SCCDate: Date;
  KCCount: number;
  KCC: string;
  KCCSites: string;
  KCCDate: Date;
  NNEOPLAT1: string;
  NNEOPLAA1: number;
  NNEOPLAY1: number;
  NNEOPLAT2: string;
  NNEOPLAA2: number;
  NNEOPLAY2: number;
  NNEOPLAT3: string;
  NNEOPLAA3: number;
  NNEOPLAY3: number;
  NNEOPLAT4: string;
  NNEOPLAA4: number;
  NNEOPLAY4: number;
  NNEOPLAT5: string;
  NNEOPLAA5: number;
  NNEOPLAY5: number;
  NNEOPLAT6: string;
  NNEOPLAA6: number;
  NNEOPLAY6: number;
  NNEOPLAT7: string;
  NNEOPLAA7: number;
  NNEOPLAY7: number;

  FHM: string;
  FHMMelanomaType: string;
  FHMMelanomaRel: string;

  GermNotTested: string;
  GermMC1R: string;
  GermCDKN2A: string;
  GermTERT: string;
  GermCDK4: string;
  GermMITF: string;
  GermBAP1: string;
  GermPOT1: string;
  GermAltro: string;

  FHMCancerType: string;
  FHMCancerRel: string;
  FHMCancerPed: string;

  constructor(solarLentigines: string, solarLentiginesAtSite: string, neviCount: string, neviScalp: number,
              neviThoraxAbdAnt: number, neviThoraxAbdBack: number, neviFaceLeft: number, neviFaceRight: number,
              neviNeckLeft: number, neviNeckRight: number, neviUpperExtremLeft: number, neviUpperExtremRight: number,
              neviLowerExtremLeft: number, neviLowerExtremRight: number, neviPalmsLeft: number, neviPalmsRight: number,
              neviSolesLeft: number, neviSolesRight: number, neviAtypical: string, neviCNMedium: string,
              neviCNMediumSites: string, neviCNLarge: string, neviCNLargeSites: string,
              neviCNGiant: string, neviCNGiantSites: string,  neviBlue: string, neviBlueCount: number,
              neviActinic: string, neviActinicSite: string, neviActinicType: string, neviKSC: string, neviKSCCount: number,
              neviKSCSites: string, neviBCC: string, neviBCCCount: number, neviBCCSites: string, neviSCC: string,
              neviSCCCount: number, neviSCCSites: string, HistoryNoCancer: string, PreviousTreatments: string,
              PregnancyNumber: number, YearOfBirthChildren: number, MiscarriagesNumber: number,
              melanomaDuringPregnancy: string, melanomaBeforePregnancy: string, melanomaBeforePregnancyY: number,
              melanomaAfterPregnancy: string, melanomaAfterPregnancyY: number, hormonBefore: string,
              BCCount: number, BCC: string, BCCSites: string, BCCDate: Date,
              SCCount: number, SCC: string, SCCSites: string, SCCDate: Date,
              KCCount: number, KCC: string, KCCSites: string, KCCDate: Date,
              NNEOPLAT1: string, NNEOPLAA1: number, NNEOPLAY1: number,
              NNEOPLAT2: string, NNEOPLAA2: number, NNEOPLAY2: number,
              NNEOPLAT3: string, NNEOPLAA3: number, NNEOPLAY3: number,
              NNEOPLAT4: string, NNEOPLAA4: number, NNEOPLAY4: number,
              NNEOPLAT5: string, NNEOPLAA5: number, NNEOPLAY5: number,
              NNEOPLAT6: string, NNEOPLAA6: number, NNEOPLAY6: number,
              NNEOPLAT7: string, NNEOPLAA7: number, NNEOPLAY7: number,
              FHM: string, FHMMelanomaType: string, FHMMelanomaRel: string, GermNotTested: string, GermMC1R: string,
              GermCDKN2A: string, GermTERT: string, GermCDK4: string, GermMITF: string,
              GermBAP1: string, GermPOT1: string, GermAltro: string,
              FHMCancerType: string, FHMCancerRel: string, FHMCancerPed: string) {
    this.solarLentigines = solarLentigines;
    this.solarLentiginesAtSite = solarLentiginesAtSite;
    this.neviCount = neviCount;
    this.neviScalp = neviScalp;
    this.neviThoraxAbdAnt = neviThoraxAbdAnt;
    this.neviThoraxAbdBack = neviThoraxAbdBack;
    this.neviFaceLeft = neviFaceLeft;
    this.neviFaceRight = neviFaceRight;
    this.neviNeckLeft = neviNeckLeft;
    this.neviNeckRight = neviNeckRight;
    this.neviUpperExtremLeft = neviUpperExtremLeft;
    this.neviUpperExtremRight = neviUpperExtremRight;
    this.neviLowerExtremLeft = neviLowerExtremLeft;
    this.neviLowerExtremRight = neviLowerExtremRight;
    this.neviPalmsLeft = neviPalmsLeft;
    this.neviPalmsRight = neviPalmsRight;
    this.neviSolesLeft = neviSolesLeft;
    this.neviSolesRight = neviSolesRight;
    this.neviAtypical = neviAtypical;
    this.neviCNMedium = neviCNMedium;
    this.neviCNMediumSites = neviCNMediumSites;
    this.neviCNLarge = neviCNLarge;
    this.neviCNLargeSites =  neviCNLargeSites;
    this.neviCNGiant = neviCNGiant;
    this.neviCNGiantSites = neviCNGiantSites;
    this.neviBlue = neviBlue;
    this.neviBlueCount = neviBlueCount;
    this.neviActinic = neviActinic;
    this.neviActinicSite = neviActinicSite;
    this.neviActinicType = neviActinicType;
    this.neviKSC = neviKSC;
    this.neviKSCCount = neviKSCCount;
    this.neviKSCSites = neviKSCSites;
    this.neviBCC = neviBCC;
    this.neviBCCCount = neviBCCCount;
    this.neviBCCSites = neviBCCSites;
    this.neviSCC = neviSCC;
    this.neviSCCCount = neviSCCCount;
    this.neviSCCSites = neviSCCSites;
    this.HistoryNoCancer = HistoryNoCancer;
    this.PreviousTreatments = PreviousTreatments;

    this.PregnancyNumber =  PregnancyNumber;
    this.YearOfBirthChildren = YearOfBirthChildren;
    this.MiscarriagesNumber = MiscarriagesNumber;
    this.melanomaDuringPregnancy = melanomaDuringPregnancy;
    this.melanomaBeforePregnancy = melanomaBeforePregnancy;
    this.melanomaBeforePregnancyY = melanomaBeforePregnancyY;
    this.melanomaAfterPregnancy = melanomaAfterPregnancy;
    this.melanomaAfterPregnancyY = melanomaAfterPregnancyY;
    this.hormonBefore = hormonBefore;
    this.BCCount = BCCount;
    this.BCC = BCC;
    this.BCCSites = BCCSites;
    this.BCCDate = BCCDate;
    this.SCCount = SCCount;
    this.SCC = SCC;
    this.SCCSites = SCCSites;
    this.SCCDate = SCCDate;
    this.KCCount = KCCount;
    this.KCC = KCC;
    this.KCCSites = KCCSites;
    this.KCCDate = KCCDate;

    this.NNEOPLAT1 = NNEOPLAT1;
    this.NNEOPLAA1 = NNEOPLAA1;
    this.NNEOPLAY1 = NNEOPLAY1;
    this.NNEOPLAT2 = NNEOPLAT2;
    this.NNEOPLAA2 = NNEOPLAA2;
    this.NNEOPLAY2 = NNEOPLAY2;
    this.NNEOPLAT3 = NNEOPLAT3;
    this.NNEOPLAA3 = NNEOPLAA3;
    this.NNEOPLAY3 = NNEOPLAY3;
    this.NNEOPLAT4 = NNEOPLAT4;
    this.NNEOPLAA4 = NNEOPLAA4;
    this.NNEOPLAY4 = NNEOPLAY4;
    this.NNEOPLAT5 = NNEOPLAT5;
    this.NNEOPLAA5 = NNEOPLAA5;
    this.NNEOPLAY5 = NNEOPLAY5;
    this.NNEOPLAT6 = NNEOPLAT6;
    this.NNEOPLAA6 = NNEOPLAA6;
    this.NNEOPLAY6 = NNEOPLAY6;
    this.NNEOPLAT7 = NNEOPLAT7;
    this.NNEOPLAA7 = NNEOPLAA7;
    this.NNEOPLAY7 = NNEOPLAY7;

    this.FHM =  FHM;
    this.FHMMelanomaType = FHMMelanomaType;
    this.FHMMelanomaRel = FHMMelanomaRel;

    this.GermNotTested = GermNotTested;
    this.GermMC1R = GermMC1R;
    this.GermCDKN2A = GermCDKN2A;
    this.GermTERT = GermTERT;
    this.GermCDK4 = GermCDK4;
    this.GermMITF = GermMITF;
    this.GermBAP1 = GermBAP1;
    this.GermPOT1 = GermPOT1;
    this.GermAltro = GermAltro;
    this.FHMCancerType = FHMCancerType;
    this.FHMCancerRel = FHMCancerRel;
    this.FHMCancerPed = FHMCancerPed;
  }
}
