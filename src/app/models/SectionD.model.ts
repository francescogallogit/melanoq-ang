export class SectionD {
  prePigmentedLesion: string;
  prePigmentedLesionY: number;
  detectionPigmentedLesion: string;
  autoDetectionPigmentedLesion: string;
  autoDetectionPigmentedLesionP: string;
  prePigmentedLesionDD: string;
  tumorSite: string;
  thicknessBreslow: number;
  subtype: string;
  micoticRate: number;
  ulceration: string;
  tumorGrowth: string;
  tumorRegression: string;
  tumorRegressionPerc: number;
  tumorInfiltrate: string;
  neviAssociated: string;
  vascularInvasion: string;
  microsatellitosis: string;
  pigmentation: string;
  solarElastosis: string;
  lateralMarginStatus: string;
  deepMarginStatus: string;
  sln: string;
  ajccStating: string;
  recordResult: string;
  sourceData: string;
  ajccSite: string;
  prePigmentedLesion1: string;
  prePigmentedLesionY1: number;
  detectionPigmentedLesion1: string;
  autoDetectionPigmentedLesion1: string;
  autoDetectionPigmentedLesionP1: string;
  prePigmentedLesionDD1: string;
  tumorSite1: string;
  thicknessBreslow1: number;
  subtype1: string;
  micoticRate1: number;
  ulceration1: string;
  tumorGrowth1: string;
  tumorRegression1: string;
  tumorRegressionPerc1: number;
  tumorInfiltrate1: string;
  neviAssociated1: string;
  vascularInvasion1: string;
  microsatellitosis1: string;
  pigmentation1: string;
  solarElastosis1: string;
  lateralMarginStatus1: string;
  deepMarginStatus1: string;
  sln1: string;
  ajccStating1: string;
  recordResult1: string;
  sourceData1: string;
  ajccSite1: string;

  constructor(prePigmentedLesion: string, prePigmentedLesionY: number, detectionPigmentedLesion: string,
              autoDetectionPigmentedLesion: string, autoDetectionPigmentedLesionP: string,
              prePigmentedLesionDD: string, tumorSite: string, thicknessBreslow: number, subtype: string,
              micoticRate: number, ulceration: string, tumorGrowth: string, tumorRegression: string,
              tumorRegressionPerc: number, tumorInfiltrate: string, neviAssociated: string,
              vascularInvasion: string, microsatellitosis: string, pigmentation: string,
              solarElastosis: string, lateralMarginStatus: string, deepMarginStatus: string, sln: string,
              ajccStating: string, recordResult: string, sourceData: string, ajccSite: string,
              prePigmentedLesion1: string, prePigmentedLesionY1: number, detectionPigmentedLesion1: string,
              autoDetectionPigmentedLesion1: string, autoDetectionPigmentedLesionP1: string,prePigmentedLesionDD1: string,
              tumorSite1: string, thicknessBreslow1: number, subtype1: string, micoticRate1: number,ulceration1: string,
              tumorGrowth1: string, tumorRegression1: string, tumorRegressionPerc1: number, tumorInfiltrate1: string,
              neviAssociated1: string, vascularInvasion1: string, microsatellitosis1: string, pigmentation1: string,
              solarElastosis1: string, lateralMarginStatus1: string, deepMarginStatus1: string, sln1: string,
              ajccStating1: string, recordResult1: string, sourceData1: string, ajccSite1: string) {
    this.prePigmentedLesion = prePigmentedLesion;
    this.prePigmentedLesionY = prePigmentedLesionY;
    this.detectionPigmentedLesion = detectionPigmentedLesion;
    this.autoDetectionPigmentedLesion = autoDetectionPigmentedLesion;
    this.autoDetectionPigmentedLesionP = autoDetectionPigmentedLesionP;
    this.prePigmentedLesionDD = prePigmentedLesionDD;
    this.tumorSite = tumorSite;
    this.thicknessBreslow = thicknessBreslow;
    this.subtype = subtype;
    this.micoticRate = micoticRate;
    this.ulceration = ulceration;
    this.tumorGrowth = tumorGrowth;
    this.tumorRegression = tumorRegression;
    this.tumorRegressionPerc = tumorRegressionPerc;
    this.tumorInfiltrate = tumorInfiltrate;
    this.neviAssociated = neviAssociated;
    this.vascularInvasion = vascularInvasion;
    this.microsatellitosis = microsatellitosis;
    this.pigmentation = pigmentation;
    this.solarElastosis = solarElastosis;
    this.lateralMarginStatus = lateralMarginStatus;
    this.deepMarginStatus = deepMarginStatus;
    this.sln = sln;
    this.ajccStating = ajccStating;
    this.recordResult = recordResult;
    this.sourceData = sourceData;
    this.ajccSite = ajccSite;
    this.prePigmentedLesion1 = prePigmentedLesion1;
    this.prePigmentedLesionY1 = prePigmentedLesionY1;
    this.detectionPigmentedLesion1 = detectionPigmentedLesion1;
    this.autoDetectionPigmentedLesion1 = autoDetectionPigmentedLesion1;
    this.autoDetectionPigmentedLesionP1 = autoDetectionPigmentedLesionP1;
    this.prePigmentedLesionDD1 = prePigmentedLesionDD1;
    this.tumorSite1 = tumorSite1;
    this.thicknessBreslow1 = thicknessBreslow1;
    this.subtype1 = subtype1;
    this.micoticRate1 = micoticRate1;
    this.ulceration1 = ulceration1;
    this.tumorGrowth1 = tumorGrowth1;
    this.tumorRegression1 = tumorRegression1;
    this.tumorRegressionPerc1 = tumorRegressionPerc1;
    this.tumorInfiltrate1 = tumorInfiltrate1;
    this.neviAssociated1 = neviAssociated1;
    this.vascularInvasion1 = vascularInvasion1;
    this.microsatellitosis1 = microsatellitosis1;
    this.pigmentation1 = pigmentation1;
    this.solarElastosis1 = solarElastosis1;
    this.lateralMarginStatus1 = lateralMarginStatus1;
    this.deepMarginStatus1 = deepMarginStatus1;
    this.sln1 = sln1;
    this.ajccStating1 = ajccStating1;
    this.recordResult1 = recordResult1;
    this.sourceData1 = sourceData1;
    this.ajccSite1 = ajccSite1;
  }
}
