import {Component, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  patients: any = [];
  selectedPatient = null;
  editedPatient = null;

  user = '';

  public showRegistration = false;

  constructor(
    private apiService: ApiService,
    private cookieService: CookieService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    const pToken = this.cookieService.get('patient-token');
    if (!pToken) {
      this.router.navigate(['/auth']);
    } else {
      this.user = this.cookieService.get('username');
      console.log("Username New:", this.user);
      // @ts-ignore
      this.apiService.getPatients().subscribe(
        data => {
          // @ts-ignore
          this.patients = data;
        },
        error => console.log(error)
      );
    }
  }
  // @ts-ignore
  selectPatient(patient) {
    this.selectedPatient = patient;
    this.editedPatient = null;
    // @ts-ignore
    //console.log('selectedPatient', this.selectedPatient, this.selectedPatient.subject);
  }

  // @ts-ignore
  editPatient(patient) {
    this.editedPatient = patient;
    this.selectedPatient = null;
    // @ts-ignore
    //console.log('selectedPatient', this.editedPatient);
  }

  createNewPatient() {
    // @ts-ignore
    this.editedPatient = {
      user: '',
      subject: '',
      dataAdministration: '',
      dataUpdated: '',
      melanomaType: '',
      sex: '',
      education: '',
      currentOccupation: '',
      dateOfBirth: '', cityOfBirth: '', provinceOfBirth: '',
      countryOfBirth: '', weight: '', height: '', ethnicity: '', skinTypeHalfTime: '', skinTypeLongTime: '',
      eyeColor: '', hairColor: '', moles: '', freckles: '', sunExposureWork: '', activitySunSpecification: '',
      activitySunSpecificationH: '', activitySunSpecificationD: '', activitySunSpecificationM: '', activitySunSpecificationY: '',
      sunExposureHobby: '', activitySunSpecificationHobby: '', activitySunSpecificationHH: '', activitySunSpecificationDH: '',
      activitySunSpecificationMH: '', activitySunSpecificationYH: '', sunExposureIntermittentUntil10W: '', sunExposureIntermittentUntil10H: '',
      sunExposureIntermittentBetween1118W: '', sunExposureIntermittentBetween1118H: '',
      sunExposureIntermittentOver18W: '', sunExposureIntermittentOver18H: '', sunExposureIntermittentPreLast10W: '', sunExposureIntermittentPreLast10H: '',
      sunExposureIntermittentLatestD: '', sunExposureIntermittentLatestM: '',

      sunburnsUnder18: '',
      sunburnsOver18: '',
      sunburnsAroundMelanoma: '',
      sunburnsLastFiveY: '',

      sunscreenUntil10: '',
      protectiveFactorUntil10: '',
      sunscreenBetween1118: '',
      protectiveFactorBetween1118: '',
      sunscreenOver18: '',
      protectiveFactorOver18: '',
      sunscreenPreLast10: '',
      protectiveFactorPreLast10: '',

      sunProtectionHat: '',
      sunProtectionClothes: '',
      sunProtectionShade: '',
      sunlamps: '',
      sunlampsTotalNumbers: '',
      sunlampsFirstSession: '',
      sunlampsLastSession: '',
      photoTherapy: '',
      smoke: '',
      smokeStart: '',
      smokeYears: '',
      smokeHow: '',
      vitamins: '',
      vitaminsHow: '',
      vitaminBCaroteneFrequency: '',
      vitaminBCaroteneDuration: '',
      vitaminAFrequency: '',
      vitaminADuration: '',
      vitaminCFrequency: '',
      vitaminCDuration: '',
      vitaminEFrequency: '',
      vitaminEDuration: '',
      vitaminDFrequency: '',
      vitaminDDuration: '',
      vitaminMultiFrequency: '',
      vitaminMultiDuration: '',
      questionAbleTo: '',
      questionIssue: '',
      questionIssueList: '',
      sectionC: '',
      sectionD: '',
      recidences: '',
      occupations: ''
    };
    this.selectedPatient = null;
  }

  deletedPatient(patient: { id: any; }) {
    // @ts-ignore
    if(confirm(`Stai per cancellare il questionario ${patient.id}`))
      this.apiService.deletePatient(patient.id).subscribe(
      data => {
        // @ts-ignore
        this.patients = this.patients.filter(pat => pat.id !== patient.id);
        console.log(data);
      },
      error => console.log(error)
    );
  }

  patientCreated(patient: any) {
    this.patients.push(patient);
    this.editedPatient = null;
  }

  patientUpdated(patient: any) {
    // @ts-ignore
    const indx = this.patients.findIndex(pat => pat.id === patient.id);
    if (indx >= 0) {
      this.patients[indx] = patient;
    }
    this.editedPatient = null;
  }

  logout() {
    this.cookieService.delete('patient-token');
    this.router.navigate(['/auth']);
  }
}
