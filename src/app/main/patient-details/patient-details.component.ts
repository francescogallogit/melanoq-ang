import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {

  @Input()
  patient: any;

  @Input()
  authForm: any;


  constructor() { }

  ngOnInit(): void {
  }



}
