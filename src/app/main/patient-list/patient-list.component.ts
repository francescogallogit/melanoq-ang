import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faUserPlus , faUserEdit, faUserMinus } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  @Input()
  patients: any = [];

  @Input()
  user: any = [];

  @Output()
  selectPatient = new EventEmitter();

  @Output()
  editedPatient = new EventEmitter();

  @Output()
  createNewPatient = new EventEmitter();

  @Output()
  deletedPatient = new EventEmitter();

  faUserPlus = faUserPlus;
  faUserMinus = faUserMinus;
  faUserEdit = faUserEdit;


  constructor() { }

  ngOnInit(): void {}

  patientClicked(patient: any) {
    //console.log(patient);
    this.selectPatient.emit(patient);
  }

  editPatient(patient: any) {
    this.editedPatient.emit(patient);
  }

  newPatient() {
    this.createNewPatient.emit();
  }

  deletePatient(patient: any) {
    this.deletedPatient.emit(patient);
  }
}
