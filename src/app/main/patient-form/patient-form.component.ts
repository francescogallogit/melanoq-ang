import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from "@angular/forms";
import { ApiService } from "../../api.service";
import {CookieService} from "ngx-cookie-service";
import {SectionC} from "../../models/SectionC.model";
import {SectionD} from "../../models/SectionD.model";
import {oldRecidences} from "../../models/oldRecidences.model";
import {oldOccupations} from "../../models/oldOccupations.model";

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit {

  // @ts-ignore
  patientForm: FormGroup;
  //@ts-ignore
  sectionCForm: FormGroup;
  //@ts-ignore
  sectionDForm: FormGroup;
  //@ts-ignore
  oldRecidencesForm: FormGroup;
  //@ts-ignore
  oldOccupationsForm: FormGroup;

  user: number = 0;
  id: number = 0;

  @Output() patientCreated = new EventEmitter();
  @Output() patientUpdated = new EventEmitter();


  @Input() set patient(val: any){
    this.id = val.id;

    this.user = Number(this.cookieService.get('patient-id'));

    console.log(this.id);
    console.log(this.user);

    this.patientForm = new FormGroup({

      user: new FormControl(this.user),

      subject: new FormControl(val.subject),
      dataAdministration: new FormControl(val.dataAdministration),
      dataUpdated: new FormControl(val.dataUpdated),
      melanomaType: new FormControl(val.melanomaType),

      sex: new FormControl(val.sex),
      education: new FormControl(val.education),
      currentOccupation: new FormControl(val.currentOccupation),
      dateOfBirth: new FormControl(val.dateOfBirth),
      cityOfBirth: new FormControl(val.cityOfBirth),
      provinceOfBirth: new FormControl(val.provinceOfBirth),
      countryOfBirth: new FormControl(val.countryOfBirth),

      weight: new FormControl(val.weight),
      height: new FormControl(val.height),
      ethnicity: new FormControl(val.ethnicity),

      skinTypeHalfTime: new FormControl(val.skinTypeHalfTime),
      skinTypeLongTime: new FormControl(val.skinTypeLongTime),

      eyeColor: new FormControl(val.eyeColor),

      hairColor: new FormControl(val.hairColor),

      moles: new FormControl(val.moles),

      freckles: new FormControl(val.freckles),

      sunExposureWork: new FormControl(val.sunExposureWork),
      activitySunSpecification: new FormControl(val.activitySunSpecification),
      activitySunSpecificationH: new FormControl(val.activitySunSpecificationH),
      activitySunSpecificationD: new FormControl(val.activitySunSpecificationD),
      activitySunSpecificationM: new FormControl(val.activitySunSpecificationM),
      activitySunSpecificationY: new FormControl(val.activitySunSpecificationY),

      sunExposureHobby: new FormControl(val.sunExposureHobby),
      activitySunSpecificationHobby: new FormControl(val.activitySunSpecificationHobby),
      activitySunSpecificationHH: new FormControl(val.activitySunSpecificationHH),
      activitySunSpecificationDH: new FormControl(val.activitySunSpecificationDH),
      activitySunSpecificationMH: new FormControl(val.activitySunSpecificationMH),
      activitySunSpecificationYH: new FormControl(val.activitySunSpecificationYH),

      sunExposureIntermittentUntil10W: new FormControl(val.sunExposureIntermittentUntil10W),
      sunExposureIntermittentUntil10H: new FormControl(val.sunExposureIntermittentUntil10H),
      sunExposureIntermittentBetween1118W: new FormControl(val.sunExposureIntermittentBetween1118W),
      sunExposureIntermittentBetween1118H: new FormControl(val.sunExposureIntermittentBetween1118H),
      sunExposureIntermittentOver18W: new FormControl(val.sunExposureIntermittentOver18W),
      sunExposureIntermittentOver18H: new FormControl(val.sunExposureIntermittentOver18H),
      sunExposureIntermittentPreLast10W: new FormControl(val.sunExposureIntermittentPreLast10W),
      sunExposureIntermittentPreLast10H: new FormControl(val.sunExposureIntermittentPreLast10H),
      sunExposureIntermittentLatestD: new FormControl(val.sunExposureIntermittentLatestD),
      sunExposureIntermittentLatestM: new FormControl(val.sunExposureIntermittentLatestM),
      sunburnsUnder18: new FormControl(val.sunburnsUnder18),
      sunburnsOver18: new FormControl(val.sunburnsOver18),
      sunburnsAroundMelanoma: new FormControl(val.sunburnsAroundMelanoma),
      sunburnsLastFiveY: new FormControl(val.sunburnsLastFiveY),

      sunscreenUntil10: new FormControl(val.sunscreenUntil10),
      protectiveFactorUntil10: new FormControl(val.protectiveFactorUntil10),
      sunscreenBetween1118: new FormControl(val.sunscreenBetween1118),
      protectiveFactorBetween1118: new FormControl(val.protectiveFactorBetween1118),
      sunscreenOver18: new FormControl(val.sunscreenOver18),
      protectiveFactorOver18: new FormControl(val.protectiveFactorOver18),
      sunscreenPreLast10: new FormControl(val.sunscreenPreLast10),
      protectiveFactorPreLast10: new FormControl(val.protectiveFactorPreLast10),

      sunProtectionHat: new FormControl(val.sunProtectionHat),
      sunProtectionClothes: new FormControl(val.sunProtectionClothes),
      sunProtectionShade: new FormControl(val.sunProtectionShade),

      sunlamps: new FormControl(val.sunlamps),
      sunlampsTotalNumbers: new FormControl(val.sunlampsTotalNumbers),
      sunlampsFirstSession: new FormControl(val.sunlampsFirstSession),
      sunlampsLastSession: new FormControl(val.sunlampsLastSession),

      photoTherapy: new FormControl(val.photoTherapy),

      smoke: new FormControl(val.smoke),
      smokeStart: new FormControl(val.smokeStart),
      smokeYears: new FormControl(val.smokeYears),
      smokeHow: new FormControl(val.smokeHow),

      vitamins: new FormControl(val.vitamins),
      vitaminsHow: new FormControl(val.vitaminsHow),
      vitaminBCaroteneFrequency: new FormControl(val.vitaminBCaroteneFrequency),
      vitaminBCaroteneDuration: new FormControl(val.vitaminBCaroteneDuration),
      vitaminAFrequency: new FormControl(val.vitaminAFrequency),
      vitaminADuration: new FormControl(val.vitaminADuration),
      vitaminCFrequency: new FormControl(val.vitaminCFrequency),
      vitaminCDuration: new FormControl(val.vitaminCDuration),
      vitaminEFrequency: new FormControl(val.vitaminEFrequency),
      vitaminEDuration: new FormControl(val.vitaminEDuration),
      vitaminDFrequency: new FormControl(val.vitaminDFrequency),
      vitaminDDuration: new FormControl(val.vitaminDDuration),
      vitaminMultiFrequency: new FormControl(val.vitaminMultiFrequency),
      vitaminMultiDuration: new FormControl(val.vitaminMultiDuration),

      questionAbleTo: new FormControl(val.questionAbleTo),
      questionIssue: new FormControl(val.questionIssue),
      questionIssueList: new FormControl(val.questionIssueList),

      sectionC: this.sectionCForm = new FormGroup({
        solarLentigines: new FormControl(val.sectionC.solarLentigines),
        solarLentiginesAtSite: new FormControl(val.sectionC.solarLentiginesAtSite),
        neviCount: new FormControl(val.sectionC.neviCount),
        neviScalp: new FormControl(val.sectionC.neviScalp),
        neviThoraxAbdAnt: new FormControl(val.sectionC.neviThoraxAbdAnt),
        neviThoraxAbdBack: new FormControl(val.sectionC.neviThoraxAbdBack),
        neviFaceLeft: new FormControl(val.sectionC.neviFaceLeft),
        neviFaceRight: new FormControl(val.sectionC.neviFaceRight),
        neviNeckLeft: new FormControl(val.sectionC.neviNeckLeft),
        neviNeckRight: new FormControl(val.sectionC.neviNeckRight),
        neviUpperExtremLeft: new FormControl(val.sectionC.neviUpperExtremLeft),
        neviUpperExtremRight: new FormControl(val.sectionC.neviUpperExtremRight),
        neviLowerExtremLeft: new FormControl(val.sectionC.neviLowerExtremLeft),
        neviLowerExtremRight: new FormControl(val.sectionC.neviLowerExtremRight),
        neviPalmsLeft: new FormControl(val.sectionC.neviPalmsLeft),
        neviPalmsRight: new FormControl(val.sectionC.neviPalmsRight),
        neviSolesLeft: new FormControl(val.sectionC.neviSolesLeft),
        neviSolesRight:  new FormControl(val.sectionC.neviSolesRight),
        neviAtypical: new FormControl(val.sectionC.neviAtypical),
        neviCNMedium: new FormControl(val.sectionC.neviCNMedium),
        neviCNMediumSites: new FormControl(val.sectionC.neviCNMediumSites),
        neviCNLarge: new FormControl(val.sectionC.neviCNLarge),
        neviCNLargeSites: new FormControl(val.sectionC.neviCNLargeSites),
        neviCNGiant: new FormControl(val.sectionC.neviCNGiant),
        neviCNGiantSites: new FormControl(val.sectionC.neviCNGiantSites),
        neviBlue: new FormControl(val.sectionC.neviBlue),
        neviBlueCount: new FormControl(val.sectionC.neviBlueCount),
        neviActinic: new FormControl(val.sectionC.neviActinic),
        neviActinicSite: new FormControl(val.sectionC.neviActinicSite),
        neviActinicType: new FormControl(val.sectionC.neviActinicType),
        neviKSC: new FormControl(val.sectionC.neviKSC),
        neviKSCCount: new FormControl(val.sectionC.neviKSCCount),
        neviKSCSites: new FormControl(val.sectionC.neviKSCSites),
        neviBCC: new FormControl(val.sectionC.neviBCC),
        neviBCCCount: new FormControl(val.sectionC.neviBCCCount),
        neviBCCSites: new FormControl(val.sectionC.neviBCCSites),
        neviSCC: new FormControl(val.sectionC.neviSCC),
        neviSCCCount: new FormControl(val.sectionC.neviSCCCount),
        neviSCCSites: new FormControl(val.sectionC.neviSCCSites),
        HistoryNoCancer: new FormControl(val.sectionC.HistoryNoCancer),
        PreviousTreatments: new FormControl(val.sectionC.PreviousTreatments),
        PregnancyNumber: new FormControl(val.sectionC.PregnancyNumber),
        YearOfBirthChildren: new FormControl(val.sectionC.YearOfBirthChildren),
        MiscarriagesNumber: new FormControl(val.sectionC.MiscarriagesNumber),
        melanomaDuringPregnancy: new FormControl(val.sectionC.melanomaDuringPregnancy),
        melanomaBeforePregnancy: new FormControl(val.sectionC.melanomaBeforePregnancy),
        melanomaBeforePregnancyY: new FormControl(val.sectionC.melanomaBeforePregnancyY),
        melanomaAfterPregnancy: new FormControl(val.sectionC.melanomaAfterPregnancy),
        melanomaAfterPregnancyY: new FormControl(val.sectionC.melanomaAfterPregnancyY),
        hormonBefore: new FormControl(val.sectionC.hormonBefore),
        BCCount: new FormControl(val.sectionC.BCCount),
        BCC: new FormControl(val.sectionC.BCC),
        BCCSites: new FormControl(val.sectionC.BCCSites),
        BCCDate: new FormControl(val.sectionC.BCCDate),
        SCCount: new FormControl(val.sectionC.SCCount),
        SCC: new FormControl(val.sectionC.SCC),
        SCCSites: new FormControl(val.sectionC.SCCSites),
        SCCDate: new FormControl(val.sectionC.SCCDate),
        KCCount: new FormControl(val.sectionC.KCCount),
        KCC: new FormControl(val.sectionC.KCC),
        KCCSites: new FormControl(val.sectionC.KCCSites),
        KCCDate: new FormControl(val.sectionC.KCCDate),
        NNEOPLAT1: new FormControl(val.sectionC.NNEOPLAT1),
        NNEOPLAA1: new FormControl(val.sectionC.NNEOPLAA1),
        NNEOPLAY1: new FormControl(val.sectionC.NNEOPLAY1),
        NNEOPLAT2: new FormControl(val.sectionC.NNEOPLAT2),
        NNEOPLAA2: new FormControl(val.sectionC.NNEOPLAA2),
        NNEOPLAY2: new FormControl(val.sectionC.NNEOPLAY2),
        NNEOPLAT3: new FormControl(val.sectionC.NNEOPLAT3),
        NNEOPLAA3: new FormControl(val.sectionC.NNEOPLAA3),
        NNEOPLAY3: new FormControl(val.sectionC.NNEOPLAY3),
        NNEOPLAT4: new FormControl(val.sectionC.NNEOPLAT4),
        NNEOPLAA4: new FormControl(val.sectionC.NNEOPLAA4),
        NNEOPLAY4: new FormControl(val.sectionC.NNEOPLAY4),
        NNEOPLAT5: new FormControl(val.sectionC.NNEOPLAT5),
        NNEOPLAA5: new FormControl(val.sectionC.NNEOPLAA5),
        NNEOPLAY5: new FormControl(val.sectionC.NNEOPLAY5),
        NNEOPLAT6: new FormControl(val.sectionC.NNEOPLAT6),
        NNEOPLAA6: new FormControl(val.sectionC.NNEOPLAA6),
        NNEOPLAY6: new FormControl(val.sectionC.NNEOPLAY6),
        NNEOPLAT7: new FormControl(val.sectionC.NNEOPLAT7),
        NNEOPLAA7: new FormControl(val.sectionC.NNEOPLAA7),
        NNEOPLAY7: new FormControl(val.sectionC.NNEOPLAY7),

        FHM: new FormControl(val.sectionC.FHM),
        FHMMelanomaType: new FormControl(val.sectionC.FHMMelanomaType),
        FHMMelanomaRel: new FormControl(val.sectionC.FHMMelanomaRel),
        GermNotTested: new FormControl(val.sectionC.GermNotTested),
        GermMC1R: new FormControl(val.sectionC.GermMC1R),
        GermCDKN2A: new FormControl(val.sectionC.GermCDKN2A),
        GermTERT: new FormControl(val.sectionC.GermTERT),
        GermCDK4: new FormControl(val.sectionC.GermCDK4),
        GermMITF: new FormControl(val.sectionC.GermMITF),
        GermBAP1: new FormControl(val.sectionC.GermBAP1),
        GermPOT1: new FormControl(val.sectionC.GermPOT1),
        GermAltro: new FormControl(val.sectionC.GermAltro),
        FHMCancerType: new FormControl(val.sectionC.FHMCancerType),
        FHMCancerRel: new FormControl(val.sectionC.FHMCancerRel),
        FHMCancerPed: new FormControl(val.sectionC.FHMCancerPed)
      }),
      sectionD: this.sectionDForm = new FormGroup({
        prePigmentedLesion: new FormControl(val.sectionD.prePigmentedLesion),
        prePigmentedLesionY: new FormControl(val.sectionD.prePigmentedLesionY),
        detectionPigmentedLesion: new FormControl(val.sectionD.detectionPigmentedLesion),
        autoDetectionPigmentedLesion: new FormControl(val.sectionD.autoDetectionPigmentedLesion),
        autoDetectionPigmentedLesionP:  new FormControl(val.sectionD.autoDetectionPigmentedLesionP),
        prePigmentedLesionDD: new FormControl(val.sectionD.prePigmentedLesionDD),
        tumorSite: new FormControl(val.sectionD.tumorSite),
        thicknessBreslow: new FormControl(val.sectionD.thicknessBreslow),
        subtype: new FormControl(val.sectionD.subtype),
        micoticRate: new FormControl(val.sectionD.micoticRate),
        ulceration: new FormControl(val.sectionD.ulceration),
        tumorGrowth: new FormControl(val.sectionD.tumorGrowth),
        tumorRegression: new FormControl(val.sectionD.tumorRegression),
        tumorRegressionPerc: new FormControl(val.sectionD.tumorRegressionPerc),
        tumorInfiltrate: new FormControl(val.sectionD.tumorInfiltrate),
        neviAssociated: new FormControl(val.sectionD.neviAssociated),
        vascularInvasion: new FormControl(val.sectionD.vascularInvasion),
        microsatellitosis: new FormControl(val.sectionD.microsatellitosis),
        pigmentation: new FormControl(val.sectionD.pigmentation),
        solarElastosis: new FormControl(val.sectionD.solarElastosis),
        lateralMarginStatus: new FormControl(val.sectionD.lateralMarginStatus),
        deepMarginStatus: new FormControl(val.sectionD.deepMarginStatus),
        sln: new FormControl(val.sectionD.sln),
        ajccStating: new FormControl(val.sectionD.ajccStating),
        recordResult: new FormControl(val.sectionD.recordResult),
        sourceData: new FormControl(val.sectionD.sourceData),
        ajccSite: new FormControl(val.sectionD.ajccSite),
        prePigmentedLesion1: new FormControl(val.sectionD.prePigmentedLesion1),
        prePigmentedLesionY1: new FormControl(val.sectionD.prePigmentedLesionY1),
        detectionPigmentedLesion1: new FormControl(val.sectionD.detectionPigmentedLesion1),
        autoDetectionPigmentedLesion1: new FormControl(val.sectionD.autoDetectionPigmentedLesion1),
        autoDetectionPigmentedLesionP1:  new FormControl(val.sectionD.autoDetectionPigmentedLesionP1),
        prePigmentedLesionDD1: new FormControl(val.sectionD.prePigmentedLesionDD1),
        tumorSite1: new FormControl(val.sectionD.tumorSite1),
        thicknessBreslow1: new FormControl(val.sectionD.thicknessBreslow1),
        subtype1: new FormControl(val.sectionD.subtype1),
        micoticRate1: new FormControl(val.sectionD.micoticRate1),
        ulceration1: new FormControl(val.sectionD.ulceration1),
        tumorGrowth1: new FormControl(val.sectionD.tumorGrowth1),
        tumorRegression1: new FormControl(val.sectionD.tumorRegression1),
        tumorRegressionPerc1: new FormControl(val.sectionD.tumorRegressionPerc1),
        tumorInfiltrate1: new FormControl(val.sectionD.tumorInfiltrate1),
        neviAssociated1: new FormControl(val.sectionD.neviAssociated1),
        vascularInvasion1: new FormControl(val.sectionD.vascularInvasion1),
        microsatellitosis1: new FormControl(val.sectionD.microsatellitosis1),
        pigmentation1: new FormControl(val.sectionD.pigmentation1),
        solarElastosis1: new FormControl(val.sectionD.solarElastosis1),
        lateralMarginStatus1: new FormControl(val.sectionD.lateralMarginStatus1),
        deepMarginStatus1: new FormControl(val.sectionD.deepMarginStatus1),
        sln1: new FormControl(val.sectionD.sln1),
        ajccStating1: new FormControl(val.sectionD.ajccStating1),
        recordResult1: new FormControl(val.sectionD.recordResult1),
        sourceData1: new FormControl(val.sectionD.sourceData1),
        ajccSite1: new FormControl(val.sectionD.ajccSite1)
      }),
      recidences: this.oldRecidencesForm = new FormGroup({
        countryResidency: new FormControl(val.recidences.countryResidency),
        cityResidency: new FormControl(val.recidences.cityResidency),
        dateFrom: new FormControl(val.recidences.dateFrom),
        dateTo: new FormControl(val.recidences.dateTo),
        countryResidency1: new FormControl(val.recidences.countryResidency1),
        cityResidency1: new FormControl(val.recidences.cityResidency1),
        dateFrom1: new FormControl(val.recidences.dateFrom1),
        dateTo1: new FormControl(val.recidences.dateTo1),
        countryResidency2: new FormControl(val.recidences.countryResidency2),
        cityResidency2: new FormControl(val.recidences.cityResidency2),
        dateFrom2: new FormControl(val.recidences.dateFrom2),
        dateTo2: new FormControl(val.recidences.dateTo2),
        countryResidency3: new FormControl(val.recidences.countryResidency3),
        cityResidency3: new FormControl(val.recidences.cityResidency3),
        dateFrom3: new FormControl(val.recidences.dateFrom3),
        dateTo3: new FormControl(val.recidences.dateTo3)
      }),

      occupations: this.oldOccupationsForm = new FormGroup({
        pastOccupationO: new FormControl(val.occupations.pastOccupationO),
        dateFromPO: new FormControl(val.occupations.dateFromPO),
        dateToPO: new FormControl(val.occupations.dateToPO),
        pastOccupationO1: new FormControl(val.occupations.pastOccupationO1),
        dateFromPO1: new FormControl(val.occupations.dateFromPO1),
        dateToPO1: new FormControl(val.occupations.dateToPO1),
        pastOccupationO2: new FormControl(val.occupations.pastOccupationO2),
        dateFromPO2: new FormControl(val.occupations.dateFromPO2),
        dateToPO2: new FormControl(val.occupations.dateToPO2),
        pastOccupationO3: new FormControl(val.occupations.pastOccupationO3),
        dateFromPO3: new FormControl(val.occupations.dateFromPO3),
        dateToPO3: new FormControl(val.occupations.dateToPO3),
      })

    });

  }
  constructor(
    private apiService: ApiService,
    private cookieService: CookieService,
  ) {  }

  ngOnInit(): void {
  }

  saveForm() {

    let occupations: oldOccupations = new oldOccupations(
      this.oldOccupationsForm.value.pastOccupationO,
      this.oldOccupationsForm.value.dateFromPO,
      this.oldOccupationsForm.value.dateToPO,
      this.oldOccupationsForm.value.pastOccupationO1,
      this.oldOccupationsForm.value.dateFromPO1,
      this.oldOccupationsForm.value.dateToPO1,
      this.oldOccupationsForm.value.pastOccupationO2,
      this.oldOccupationsForm.value.dateFromPO2,
      this.oldOccupationsForm.value.dateToPO2,
      this.oldOccupationsForm.value.pastOccupationO3,
      this.oldOccupationsForm.value.dateFromPO3,
      this.oldOccupationsForm.value.dateToPO3
    )

    let recidences: oldRecidences = new oldRecidences(
      this.oldRecidencesForm.value.countryResidency,
      this.oldRecidencesForm.value.cityResidency,
      this.oldRecidencesForm.value.dateFrom,
      this.oldRecidencesForm.value.dateTo,
      this.oldRecidencesForm.value.countryResidency1,
      this.oldRecidencesForm.value.cityResidency1,
      this.oldRecidencesForm.value.dateFrom1,
      this.oldRecidencesForm.value.dateTo1,
      this.oldRecidencesForm.value.countryResidency2,
      this.oldRecidencesForm.value.cityResidency2,
      this.oldRecidencesForm.value.dateFrom2,
      this.oldRecidencesForm.value.dateTo2,
      this.oldRecidencesForm.value.countryResidency3,
      this.oldRecidencesForm.value.cityResidency3,
      this.oldRecidencesForm.value.dateFrom3,
      this.oldRecidencesForm.value.dateTo3
    )

    let sectionC: SectionC = new SectionC(
      this.sectionCForm.value.solarLentigines, this.sectionCForm.value.solarLentiginesAtSite,
      this.sectionCForm.value.neviCount, this.sectionCForm.value.neviScalp, this.sectionCForm.value.neviThoraxAbdAnt,
      this.sectionCForm.value.neviThoraxAbdBack, this.sectionCForm.value.neviFaceLeft, this.sectionCForm.value.neviFaceRight,
      this.sectionCForm.value.neviNeckLeft, this.sectionCForm.value.neviNeckRight, this.sectionCForm.value.neviUpperExtremLeft,
      this.sectionCForm.value.neviUpperExtremRight, this.sectionCForm.value.neviLowerExtremLeft, this.sectionCForm.value.neviLowerExtremRight,
      this.sectionCForm.value.neviPalmsLeft, this.sectionCForm.value.neviPalmsRight, this.sectionCForm.value.neviSolesLeft,
      this.sectionCForm.value.neviSolesRight, this.sectionCForm.value.neviAtypical, this.sectionCForm.value.neviCNMedium,
      this.sectionCForm.value.neviCNMediumSites, this.sectionCForm.value.neviCNLarge, this.sectionCForm.value.neviCNLargeSites,
      this.sectionCForm.value.neviCNGiant, this.sectionCForm.value.neviCNGiantSites, this.sectionCForm.value.neviBlue,
      this.sectionCForm.value.neviBlueCount, this.sectionCForm.value.neviActinic, this.sectionCForm.value.neviActinicSite,
      this.sectionCForm.value.neviActinicType, this.sectionCForm.value.neviKSC, this.sectionCForm.value.neviKSCCount,
      this.sectionCForm.value.neviKSCSites, this.sectionCForm.value.neviBCC, this.sectionCForm.value.neviBCCCount,
      this.sectionCForm.value.neviBCCSites, this.sectionCForm.value.neviSCC, this.sectionCForm.value.neviSCCCount,
      this.sectionCForm.value.neviSCCSites, this.sectionCForm.value.HistoryNoCancer, this.sectionCForm.value.PreviousTreatments,
      this.sectionCForm.value.PregnancyNumber, this.sectionCForm.value.YearOfBirthChildren, this.sectionCForm.value.MiscarriagesNumber,
      this.sectionCForm.value.melanomaDuringPregnancy, this.sectionCForm.value.melanomaBeforePregnancy,
      this.sectionCForm.value.melanomaBeforePregnancyY, this.sectionCForm.value.melanomaAfterPregnancy,
      this.sectionCForm.value.melanomaAfterPregnancyY, this.sectionCForm.value.hormonBefore,
      this.sectionCForm.value.BCCount, this.sectionCForm.value.BCC, this.sectionCForm.value.BCCSites,
      this.sectionCForm.value.BCCDate,
      this.sectionCForm.value.SCCount, this.sectionCForm.value.SCC, this.sectionCForm.value.SCCSites,
      this.sectionCForm.value.SCCDate,
      this.sectionCForm.value.KCCount, this.sectionCForm.value.KCC, this.sectionCForm.value.KCCSites,
      this.sectionCForm.value.KCCDate,
      this.sectionCForm.value.NNEOPLAT1, this.sectionCForm.value.NNEOPLAA1, this.sectionCForm.value.NNEOPLAY1,
      this.sectionCForm.value.NNEOPLAT2, this.sectionCForm.value.NNEOPLAA2, this.sectionCForm.value.NNEOPLAY2,
      this.sectionCForm.value.NNEOPLAT3, this.sectionCForm.value.NNEOPLAA3, this.sectionCForm.value.NNEOPLAY3,
      this.sectionCForm.value.NNEOPLAT4, this.sectionCForm.value.NNEOPLAA4, this.sectionCForm.value.NNEOPLAY4,
      this.sectionCForm.value.NNEOPLAT5, this.sectionCForm.value.NNEOPLAA5, this.sectionCForm.value.NNEOPLAY5,
      this.sectionCForm.value.NNEOPLAT6, this.sectionCForm.value.NNEOPLAA6, this.sectionCForm.value.NNEOPLAY6,
      this.sectionCForm.value.NNEOPLAT7, this.sectionCForm.value.NNEOPLAA7, this.sectionCForm.value.NNEOPLAY7,
      this.sectionCForm.value.FHM, this.sectionCForm.value.FHMMelanomaType, this.sectionCForm.value.FHMMelanomaRel,
      this.sectionCForm.value.GermNotTested, this.sectionCForm.value.GermMC1R, this.sectionCForm.value.GermCDKN2A,
      this.sectionCForm.value.GermTERT, this.sectionCForm.value.GermCDK4, this.sectionCForm.value.GermMITF,
      this.sectionCForm.value.GermBAP1, this.sectionCForm.value.GermPOT1, this.sectionCForm.value.GermAltro,
      this.sectionCForm.value.FHMCancerType, this.sectionCForm.value.FHMCancerRel, this.sectionCForm.value.FHMCancerPed)

    let sectionD: SectionD = new SectionD(
      this.sectionDForm.value.prePigmentedLesion, this.sectionDForm.value.prePigmentedLesionY,
      this.sectionDForm.value.detectionPigmentedLesion, this.sectionDForm.value.autoDetectionPigmentedLesion,
      this.sectionDForm.value.autoDetectionPigmentedLesionP, this.sectionDForm.value.prePigmentedLesionDD,
      this.sectionDForm.value.tumorSite, this.sectionDForm.value.thicknessBreslow, this.sectionDForm.value.subtype,
      this.sectionDForm.value.micoticRate, this.sectionDForm.value.ulceration, this.sectionDForm.value.tumorGrowth,
      this.sectionDForm.value.tumorRegression, this.sectionDForm.value.tumorRegressionPerc, this.sectionDForm.value.tumorInfiltrate,
      this.sectionDForm.value.neviAssociated, this.sectionDForm.value.vascularInvasion, this.sectionDForm.value.microsatellitosis,
      this.sectionDForm.value.pigmentation, this.sectionDForm.value.solarElastosis, this.sectionDForm.value.lateralMarginStatus,
      this.sectionDForm.value.deepMarginStatus, this.sectionDForm.value.sln, this.sectionDForm.value.ajccStating,
      this.sectionDForm.value.recordResult, this.sectionDForm.value.sourceData, this.sectionDForm.value.ajccSite,
      this.sectionDForm.value.prePigmentedLesion1, this.sectionDForm.value.prePigmentedLesionY1,
      this.sectionDForm.value.detectionPigmentedLesion1, this.sectionDForm.value.autoDetectionPigmentedLesion1,
      this.sectionDForm.value.autoDetectionPigmentedLesionP1, this.sectionDForm.value.prePigmentedLesionDD1,
      this.sectionDForm.value.tumorSite1, this.sectionDForm.value.thicknessBreslow1, this.sectionDForm.value.subtype1,
      this.sectionDForm.value.micoticRate1, this.sectionDForm.value.ulceration1, this.sectionDForm.value.tumorGrowth1,
      this.sectionDForm.value.tumorRegression1, this.sectionDForm.value.tumorRegressionPerc1, this.sectionDForm.value.tumorInfiltrate1,
      this.sectionDForm.value.neviAssociated1, this.sectionDForm.value.vascularInvasion1, this.sectionDForm.value.microsatellitosis1,
      this.sectionDForm.value.pigmentation1, this.sectionDForm.value.solarElastosis1, this.sectionDForm.value.lateralMarginStatus1,
      this.sectionDForm.value.deepMarginStatus1, this.sectionDForm.value.sln1, this.sectionDForm.value.ajccStating1,
      this.sectionDForm.value.recordResult1, this.sectionDForm.value.sourceData1, this.sectionDForm.value.ajccSite1
    )

    console.log(this.patientForm.value);
    if (this.id) {
      // @ts-ignore
      this.apiService.updatePatient(
        this.id,
        this.user,
        this.patientForm.value.subject, this.patientForm.value.dataAdministration, this.patientForm.value.dataUpdated,
        this.patientForm.value.melanomaType, this.patientForm.value.sex, this.patientForm.value.education,
        this.patientForm.value.currentOccupation, this.patientForm.value.dateOfBirth, this.patientForm.value.cityOfBirth,
        this.patientForm.value.provinceOfBirth, this.patientForm.value.countryOfBirth, this.patientForm.value.weight,
        this.patientForm.value.height, this.patientForm.value.ethnicity, this.patientForm.value.skinTypeHalfTime,
        this.patientForm.value.skinTypeLongTime, this.patientForm.value.eyeColor, this.patientForm.value.hairColor,
        this.patientForm.value.moles, this.patientForm.value.freckles, this.patientForm.value.sunExposureWork,
        this.patientForm.value.activitySunSpecification, this.patientForm.value.activitySunSpecificationH,
        this.patientForm.value.activitySunSpecificationD, this.patientForm.value.activitySunSpecificationM,
        this.patientForm.value.activitySunSpecificationY, this.patientForm.value.sunExposureHobby,
        this.patientForm.value.activitySunSpecificationHobby, this.patientForm.value.activitySunSpecificationHH,
        this.patientForm.value.activitySunSpecificationDH, this.patientForm.value.activitySunSpecificationMH,
        this.patientForm.value.activitySunSpecificationYH, this.patientForm.value.sunExposureIntermittentUntil10W,
        this.patientForm.value.sunExposureIntermittentUntil10H, this.patientForm.value.sunExposureIntermittentBetween1118W,
        this.patientForm.value.sunExposureIntermittentBetween1118H, this.patientForm.value.sunExposureIntermittentOver18W,
        this.patientForm.value.sunExposureIntermittentOver18H,

        this.patientForm.value.sunExposureIntermittentPreLast10W,
        this.patientForm.value.sunExposureIntermittentPreLast10H, this.patientForm.value.sunExposureIntermittentLatestD,
        this.patientForm.value.sunExposureIntermittentLatestM, this.patientForm.value.sunburnsUnder18,
        this.patientForm.value.sunburnsOver18, this.patientForm.value.sunburnsAroundMelanoma,
        this.patientForm.value.sunburnsLastFiveY,
        this.patientForm.value.sunscreenUntil10,
        this.patientForm.value.protectiveFactorUntil10,
        this.patientForm.value.sunscreenBetween1118,
        this.patientForm.value.protectiveFactorBetween1118,
        this.patientForm.value.sunscreenOver18,
        this.patientForm.value.protectiveFactorOver18,
        this.patientForm.value.sunscreenPreLast10,
        this.patientForm.value.protectiveFactorPreLast10,
        this.patientForm.value.sunProtectionHat,
        this.patientForm.value.sunProtectionClothes,
        this.patientForm.value.sunProtectionShade,
        this.patientForm.value.sunlamps,
        this.patientForm.value.sunlampsTotalNumbers,
        this.patientForm.value.sunlampsFirstSession,
        this.patientForm.value.sunlampsLastSession,
        this.patientForm.value.photoTherapy,
        this.patientForm.value.smoke,
        this.patientForm.value.smokeStart,
        this.patientForm.value.smokeYears,
        this.patientForm.value.smokeHow,
        this.patientForm.value.vitamins,
        this.patientForm.value.vitaminsHow,
        this.patientForm.value.vitaminBCaroteneFrequency,
        this.patientForm.value.vitaminBCaroteneDuration, this.patientForm.value.vitaminAFrequency,
        this.patientForm.value.vitaminADuration, this.patientForm.value.vitaminCFrequency,
        this.patientForm.value.vitaminCDuration, this.patientForm.value.vitaminEFrequency,

        this.patientForm.value.vitaminEDuration, this.patientForm.value.vitaminDFrequency,
        this.patientForm.value.vitaminDDuration, this.patientForm.value.vitaminMultiFrequency,
        this.patientForm.value.vitaminMultiDuration, this.patientForm.value.questionAbleTo,
        this.patientForm.value.questionIssue, this.patientForm.value.questionIssueList,
        sectionC, sectionD, recidences, occupations).subscribe(
        (result: any) => this.patientUpdated.emit(result),
        error => console.log(error)
      );
    } else {
      // @ts-ignore
      this.apiService.createPatient(
        this.user,
        this.patientForm.value.subject, this.patientForm.value.dataAdministration, this.patientForm.value.dataUpdated,
        this.patientForm.value.melanomaType, this.patientForm.value.sex, this.patientForm.value.education,
        this.patientForm.value.currentOccupation, this.patientForm.value.dateOfBirth, this.patientForm.value.cityOfBirth,
        this.patientForm.value.provinceOfBirth, this.patientForm.value.countryOfBirth, this.patientForm.value.weight,
        this.patientForm.value.height, this.patientForm.value.ethnicity, this.patientForm.value.skinTypeHalfTime,
        this.patientForm.value.skinTypeLongTime, this.patientForm.value.eyeColor, this.patientForm.value.hairColor,
        this.patientForm.value.moles, this.patientForm.value.freckles, this.patientForm.value.sunExposureWork,
        this.patientForm.value.activitySunSpecification, this.patientForm.value.activitySunSpecificationH,
        this.patientForm.value.activitySunSpecificationD, this.patientForm.value.activitySunSpecificationM,
        this.patientForm.value.activitySunSpecificationY, this.patientForm.value.sunExposureHobby,
        this.patientForm.value.activitySunSpecificationHobby, this.patientForm.value.activitySunSpecificationHH,
        this.patientForm.value.activitySunSpecificationDH, this.patientForm.value.activitySunSpecificationMH,
        this.patientForm.value.activitySunSpecificationYH, this.patientForm.value.sunExposureIntermittentUntil10W,
        this.patientForm.value.sunExposureIntermittentUntil10H, this.patientForm.value.sunExposureIntermittentBetween1118W,
        this.patientForm.value.sunExposureIntermittentBetween1118H, this.patientForm.value.sunExposureIntermittentOver18W,
        this.patientForm.value.sunExposureIntermittentOver18H, this.patientForm.value.sunExposureIntermittentPreLast10W,
        this.patientForm.value.sunExposureIntermittentPreLast10H, this.patientForm.value.sunExposureIntermittentLatestD,
        this.patientForm.value.sunExposureIntermittentLatestM, this.patientForm.value.sunburnsUnder18,
        this.patientForm.value.sunburnsOver18, this.patientForm.value.sunburnsAroundMelanoma,
        this.patientForm.value.sunburnsLastFiveY,

        this.patientForm.value.sunscreenUntil10,
        this.patientForm.value.protectiveFactorUntil10,
        this.patientForm.value.sunscreenBetween1118,
        this.patientForm.value.protectiveFactorBetween1118,
        this.patientForm.value.sunscreenOver18,
        this.patientForm.value.protectiveFactorOver18,
        this.patientForm.value.sunscreenPreLast10,
        this.patientForm.value.protectiveFactorPreLast10,

        this.patientForm.value.sunProtectionHat,
        this.patientForm.value.sunProtectionClothes,
        this.patientForm.value.sunProtectionShade,
        this.patientForm.value.sunlamps,
        this.patientForm.value.sunlampsTotalNumbers,
        this.patientForm.value.sunlampsFirstSession,
        this.patientForm.value.sunlampsLastSession,
        this.patientForm.value.photoTherapy,

        this.patientForm.value.smoke,
        this.patientForm.value.smokeStart,
        this.patientForm.value.smokeYears,
        this.patientForm.value.smokeHow,
        this.patientForm.value.vitamins,
        this.patientForm.value.vitaminsHow,

        this.patientForm.value.vitaminBCaroteneFrequency,
        this.patientForm.value.vitaminBCaroteneDuration, this.patientForm.value.vitaminAFrequency,
        this.patientForm.value.vitaminADuration, this.patientForm.value.vitaminCFrequency,
        this.patientForm.value.vitaminCDuration, this.patientForm.value.vitaminEFrequency,
        this.patientForm.value.vitaminEDuration, this.patientForm.value.vitaminDFrequency,
        this.patientForm.value.vitaminDDuration, this.patientForm.value.vitaminMultiFrequency,
        this.patientForm.value.vitaminMultiDuration, this.patientForm.value.questionAbleTo,
        this.patientForm.value.questionIssue, this.patientForm.value.questionIssueList,
        sectionC, sectionD, recidences, occupations).subscribe(
        (result: any) => this.patientCreated.emit(result),
        error => console.log(error)
      );
    }


  }
}
