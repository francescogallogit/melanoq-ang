import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';

import { Routes, RouterModule } from "@angular/router";

import { ApiService } from "../api.service";

import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserComponent} from "../user/user.component";

const routes: Routes = [
  { path: 'patients', component: MainComponent }
];

@NgModule({
  declarations: [
    MainComponent,
    PatientListComponent,
    PatientDetailsComponent,
    PatientFormComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule
  ],
    exports: [
        RouterModule,
        MainComponent,
        UserComponent
    ],
  providers: [
    ApiService
  ]
})
export class MainModule {

}
