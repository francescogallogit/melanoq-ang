import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

import {CookieService} from 'ngx-cookie-service';
import {SectionC} from "./models/SectionC.model";
import {SectionD} from "./models/SectionD.model";
import {oldRecidences} from "./models/oldRecidences.model";
import {oldOccupations} from "./models/oldOccupations.model";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'https://melanostrumaq.herokuapp.com/';
  basePatientUrl = `${this.baseUrl}api/patients/`;
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) {
  }

  getPatients() {
    return this.httpClient.get(this.basePatientUrl, {headers: this.getAuthHeaders()});
  }

  getPatient(id: number) {
    return this.httpClient.get(`${this.basePatientUrl}${id}/`, {headers: this.getAuthHeaders()});
  }

  createPatient(user: number, subject: string, dataAdministration: Date, dataUpdated: Date, melanomaType: string, sex: string,
                education: string, currentOccupation: string, dateOfBirth: Date, cityOfBirth: Date,
                provinceOfBirth: string, countryOfBirth: string, weight: number, height: number, ethnicity: string,
                skinTypeHalfTime: string, skinTypeLongTime: string, eyeColor: string, hairColor: string, moles: string,
                freckles: string, sunExposureWork: string, activitySunSpecification: string, activitySunSpecificationH: number,
                activitySunSpecificationD: number, activitySunSpecificationM: number, activitySunSpecificationY: number,

                sunExposureHobby: string, activitySunSpecificationHobby: string, activitySunSpecificationHH: number,
                activitySunSpecificationDH: number, activitySunSpecificationMH: number, activitySunSpecificationYH: number,
                sunExposureIntermittentUntil10W: number, sunExposureIntermittentUntil10H: number,
                sunExposureIntermittentBetween1118W: number, sunExposureIntermittentBetween1118H: number,
                sunExposureIntermittentOver18W: number, sunExposureIntermittentOver18H: number,
                sunExposureIntermittentPreLast10W: number, sunExposureIntermittentPreLast10H: number,
                sunExposureIntermittentLatestD: Date, sunExposureIntermittentLatestM: number,
                sunburnsUnder18: string,
                sunburnsOver18: string, sunburnsAroundMelanoma: string, sunburnsLastFiveY: string,

                sunscreenUntil10: string, protectiveFactorUntil10: string,
                sunscreenBetween1118: string, protectiveFactorBetween1118: string,
                sunscreenOver18: string, protectiveFactorOver18: string,
                sunscreenPreLast10: string, protectiveFactorPreLast10: string,

                sunProtectionHat: string, sunProtectionClothes: string, sunProtectionShade: string,
                sunlamps: number, sunlampsTotalNumbers: number, sunlampsFirstSession: number, sunlampsLastSession: number,
                photoTherapy: string,
                smoke: string,
                smokeStart: number,
                smokeYears: number,
                smokeHow: string,
                vitamins: string,
                vitaminsHow: string,
                vitaminBCaroteneFrequency: string,
                vitaminBCaroteneDuration: string,
                vitaminAFrequency: string,
                vitaminADuration: string,
                vitaminCFrequency: string,
                vitaminCDuration: string,
                vitaminEFrequency: string,
                vitaminEDuration: string,
                vitaminDFrequency: string,
                vitaminDDuration: string,
                vitaminMultiFrequency: string,
                vitaminMultiDuration: string,
                questionAbleTo: string,
                questionIssue: string,
                questionIssueList: string,
                sectionC: SectionC,
                sectionD: SectionD,
                recidences: oldRecidences,
                occupations: oldOccupations
                ) {
    var sub = "M0";
    /*console.log("Data: ", subject, dataAdministration, dataUpdated, melanomaType, sex, education, currentOccupation, dateOfBirth,
      cityOfBirth, provinceOfBirth, countryOfBirth, weigh, heigh, ethnicity);*/
    // @ts-ignore
    if (subject === "Controllo")
      sub = "C0"
    // @ts-ignore
    const body = JSON.stringify({
      user: user,
      subject: subject,
      dataAdministration: dataAdministration,
      dataUpdated: dataUpdated,
      melanomaType: melanomaType,
      sex: sex,
      education: education,
      currentOccupation: currentOccupation,
      dateOfBirth: dateOfBirth,
      cityOfBirth: cityOfBirth,
      provinceOfBirth: provinceOfBirth,
      countryOfBirth: countryOfBirth,
      weight: weight,
      height: height,
      ethnicity: ethnicity,
      skinTypeHalfTime: skinTypeHalfTime,
      skinTypeLongTime: skinTypeLongTime,
      eyeColor: eyeColor,
      hairColor: hairColor,
      moles: moles,
      freckles: freckles,
      sunExposureWork: sunExposureWork,
      activitySunSpecification: activitySunSpecification,
      activitySunSpecificationH: activitySunSpecificationH,
      activitySunSpecificationD: activitySunSpecificationD,
      activitySunSpecificationM: activitySunSpecificationM,
      activitySunSpecificationY: activitySunSpecificationY,
      sunExposureHobby: sunExposureHobby,
      activitySunSpecificationHobby: activitySunSpecificationHobby,
      activitySunSpecificationHH: activitySunSpecificationHH,
      activitySunSpecificationDH: activitySunSpecificationDH,
      activitySunSpecificationMH: activitySunSpecificationMH,
      activitySunSpecificationYH: activitySunSpecificationYH,
      sunExposureIntermittentUntil10W: sunExposureIntermittentUntil10W,
      sunExposureIntermittentUntil10H: sunExposureIntermittentUntil10H,
      sunExposureIntermittentBetween1118W: sunExposureIntermittentBetween1118W,
      sunExposureIntermittentBetween1118H: sunExposureIntermittentBetween1118H,
      sunExposureIntermittentOver18W: sunExposureIntermittentOver18W,
      sunExposureIntermittentOver18H: sunExposureIntermittentOver18H,
      sunExposureIntermittentPreLast10W: sunExposureIntermittentPreLast10W,
      sunExposureIntermittentPreLast10H: sunExposureIntermittentPreLast10H,
      sunExposureIntermittentLatestD: sunExposureIntermittentLatestD,
      sunExposureIntermittentLatestM: sunExposureIntermittentLatestM,
      sunburnsUnder18: sunburnsUnder18,
      sunburnsOver18: sunburnsOver18,
      sunburnsAroundMelanoma: sunburnsAroundMelanoma,
      sunburnsLastFiveY: sunburnsLastFiveY,

      sunscreenUntil10: sunscreenUntil10,
      protectiveFactorUntil10: protectiveFactorUntil10,
      sunscreenBetween1118: sunscreenBetween1118,
      protectiveFactorBetween1118: protectiveFactorBetween1118,
      sunscreenOver18: sunscreenOver18,
      protectiveFactorOver18: protectiveFactorOver18,
      sunscreenPreLast10: sunscreenPreLast10,
      protectiveFactorPreLast10: protectiveFactorPreLast10,

      sunProtectionHat: sunProtectionHat,
      sunProtectionClothes: sunProtectionClothes,
      sunProtectionShade: sunProtectionShade,
      sunlamps: sunlamps,
      sunlampsTotalNumbers: sunlampsTotalNumbers,
      sunlampsFirstSession: sunlampsFirstSession,
      sunlampsLastSession: sunlampsLastSession,
      photoTherapy: photoTherapy,

      smoke: smoke,
      smokeStart: smokeStart,
      smokeYears: smokeYears,
      smokeHow: smokeHow,
      vitamins: vitamins,
      vitaminsHow: vitaminsHow,
      vitaminBCaroteneFrequency: vitaminBCaroteneFrequency,
      vitaminBCaroteneDuration: vitaminBCaroteneDuration,
      vitaminAFrequency: vitaminAFrequency,
      vitaminADuration: vitaminADuration,
      vitaminCFrequency: vitaminCFrequency,
      vitaminCDuration: vitaminCDuration,
      vitaminEFrequency: vitaminEFrequency,
      vitaminEDuration: vitaminEDuration,
      vitaminDFrequency: vitaminDFrequency,
      vitaminDDuration: vitaminDDuration,
      vitaminMultiFrequency: vitaminMultiFrequency,
      vitaminMultiDuration: vitaminMultiDuration,
      questionAbleTo: questionAbleTo,
      questionIssue: questionIssue,
      questionIssueList: questionIssueList,
      sectionC: sectionC,
      sectionD: sectionD,
      recidences: recidences,
      occupations: occupations
    });
    //console.log("Body: ", body);
    return this.httpClient.post(`${this.basePatientUrl}`, body, {headers: this.getAuthHeaders()});
  }

  updatePatient(id: number, user: number, subject: string, dataAdministration: Date, dataUpdated: Date, melanomaType: string, sex: string,
                education: string, currentOccupation: string, dateOfBirth: Date, cityOfBirth: Date,
                provinceOfBirth: string, countryOfBirth: string, weight: number, height: number, ethnicity: string,
                skinTypeHalfTime: string, skinTypeLongTime: string, eyeColor: string, hairColor: string, moles: string, freckles: string,
                sunExposureWork: string, activitySunSpecification: string, activitySunSpecificationH: number, activitySunSpecificationD: number,
                activitySunSpecificationM: number, activitySunSpecificationY: number, sunExposureHobby: string,
                activitySunSpecificationHobby: string, activitySunSpecificationHH: number,
                activitySunSpecificationDH: number, activitySunSpecificationMH: number, activitySunSpecificationYH: number,
                sunExposureIntermittentUntil10W: number, sunExposureIntermittentUntil10H: number,
                sunExposureIntermittentBetween1118W: number, sunExposureIntermittentBetween1118H: number,
                sunExposureIntermittentOver18W: number, sunExposureIntermittentOver18H: number,
                sunExposureIntermittentPreLast10W: number, sunExposureIntermittentPreLast10H:number,
                sunExposureIntermittentLatestD: Date, sunExposureIntermittentLatestM: number, sunburnsUnder18: string,
                sunburnsOver18: string, sunburnsAroundMelanoma: string, sunburnsLastFiveY: string,

                sunscreenUntil10: string, protectiveFactorUntil10: string,
                sunscreenBetween1118: string, protectiveFactorBetween1118: string,
                sunscreenOver18: string, protectiveFactorOver18: string,
                sunscreenPreLast10: string, protectiveFactorPreLast10: string,

                sunProtectionHat: string,
                sunProtectionClothes: string,
                sunProtectionShade: string,
                sunlamps: number,
                sunlampsTotalNumbers: number,
                sunlampsFirstSession: number,
                sunlampsLastSession: number,
                photoTherapy: string,

                smoke: string,
                smokeStart: number,
                smokeYears: number,
                smokeHow: string,

                vitamins: string,
                vitaminsHow: string,
                vitaminBCaroteneFrequency: string,
                vitaminBCaroteneDuration: string,
                vitaminAFrequency: string,
                vitaminADuration: string,
                vitaminCFrequency: string,
                vitaminCDuration: string,
                vitaminEFrequency: string,
                vitaminEDuration: string,
                vitaminDFrequency: string,
                vitaminDDuration: string,
                vitaminMultiFrequency: string,
                vitaminMultiDuration: string,
                questionAbleTo: string,
                questionIssue: string,
                questionIssueList: string,
                sectionC: SectionC,
                sectionD: SectionD,
                recidences: oldRecidences,
                occupations: oldOccupations) {

    /*console.log("Data: ", subject, dataAdministration, dataUpdated, melanomaType, sex, education, currentOccupation, dateOfBirth,
      cityOfBirth, provinceOfBirth, countryOfBirth, weigh, heigh, ethnicity);*/
    // @ts-ignore
    const body = JSON.stringify({
      user: user,
      subject: subject,
      dataAdministration: dataAdministration,
      dataUpdated: dataUpdated,
      melanomaType: melanomaType,
      sex: sex,
      education: education,
      currentOccupation: currentOccupation,
      dateOfBirth: dateOfBirth,
      cityOfBirth: cityOfBirth,
      provinceOfBirth: provinceOfBirth,
      countryOfBirth: countryOfBirth,
      weight: weight,
      height: height,
      ethnicity: ethnicity,
      skinTypeHalfTime: skinTypeHalfTime,
      skinTypeLongTime: skinTypeLongTime,
      eyeColor: eyeColor,
      hairColor: hairColor,
      moles: moles,
      freckles: freckles,
      sunExposureWork: sunExposureWork,
      activitySunSpecification: activitySunSpecification,
      activitySunSpecificationH: activitySunSpecificationH,
      activitySunSpecificationD: activitySunSpecificationD,
      activitySunSpecificationM: activitySunSpecificationM,
      activitySunSpecificationY: activitySunSpecificationY,
      sunExposureHobby: sunExposureHobby,
      activitySunSpecificationHobby: activitySunSpecificationHobby,
      activitySunSpecificationHH: activitySunSpecificationHH,
      activitySunSpecificationDH: activitySunSpecificationDH,
      activitySunSpecificationMH: activitySunSpecificationMH,
      activitySunSpecificationYH: activitySunSpecificationYH,
      sunExposureIntermittentUntil10W: sunExposureIntermittentUntil10W,
      sunExposureIntermittentUntil10H: sunExposureIntermittentUntil10H,
      sunExposureIntermittentBetween1118W: sunExposureIntermittentBetween1118W,
      sunExposureIntermittentBetween1118H: sunExposureIntermittentBetween1118H,
      sunExposureIntermittentOver18W: sunExposureIntermittentOver18W,
      sunExposureIntermittentOver18H: sunExposureIntermittentOver18H,
      sunExposureIntermittentPreLast10W: sunExposureIntermittentPreLast10W,
      sunExposureIntermittentPreLast10H: sunExposureIntermittentPreLast10H,
      sunExposureIntermittentLatestD: sunExposureIntermittentLatestD,
      sunExposureIntermittentLatestM: sunExposureIntermittentLatestM,
      sunburnsUnder18: sunburnsUnder18,
      sunburnsOver18: sunburnsOver18,
      sunburnsAroundMelanoma: sunburnsAroundMelanoma,
      sunburnsLastFiveY: sunburnsLastFiveY,
      sunscreenUntil10: sunscreenUntil10,
      protectiveFactorUntil10: protectiveFactorUntil10,
      sunscreenBetween1118: sunscreenBetween1118,
      protectiveFactorBetween1118: protectiveFactorBetween1118,
      sunscreenOver18: sunscreenOver18,
      protectiveFactorOver18: protectiveFactorOver18,
      sunscreenPreLast10: sunscreenPreLast10,
      protectiveFactorPreLast10: protectiveFactorPreLast10,

      sunProtectionHat: sunProtectionHat,
      sunProtectionClothes: sunProtectionClothes,
      sunProtectionShade: sunProtectionShade,
      sunlamps: sunlamps,
      sunlampsTotalNumbers: sunlampsTotalNumbers,
      sunlampsFirstSession: sunlampsFirstSession,
      sunlampsLastSession: sunlampsLastSession,
      photoTherapy: photoTherapy,

      smoke: smoke,
      smokeStart: smokeStart,
      smokeYears: smokeYears,
      smokeHow: smokeHow,
      vitamins: vitamins,
      vitaminsHow: vitaminsHow,
      vitaminBCaroteneFrequency: vitaminBCaroteneFrequency,
      vitaminBCaroteneDuration: vitaminBCaroteneDuration,
      vitaminAFrequency: vitaminAFrequency,
      vitaminADuration: vitaminADuration,
      vitaminCFrequency: vitaminCFrequency,
      vitaminCDuration: vitaminCDuration,
      vitaminEFrequency: vitaminEFrequency,
      vitaminEDuration: vitaminEDuration,
      vitaminDFrequency: vitaminDFrequency,
      vitaminDDuration: vitaminDDuration,
      vitaminMultiFrequency: vitaminMultiFrequency,
      vitaminMultiDuration: vitaminMultiDuration,
      questionAbleTo: questionAbleTo,
      questionIssue: questionIssue,
      questionIssueList: questionIssueList,
      sectionC: sectionC,
      sectionD: sectionD,
      recidences: recidences,
      occupations: occupations
    });
    //console.log("Body: ", body);
    return this.httpClient.put(`${this.basePatientUrl}${id}/`, body, {headers: this.getAuthHeaders()});
  }

  deletePatient(id: number) {

    return this.httpClient.delete(`${this.basePatientUrl}${id}/`, {headers: this.getAuthHeaders()});
  }

  loginUser(authData: any) {
    const body = JSON.stringify(authData);
    //console.log("Body: ", body);
    return this.httpClient.post(`${this.baseUrl}auth/`, body, {headers: this.headers});
  }

  createUser(username: string, password: string) {
    const body = JSON.stringify({
      username: username,
      password: password
    });
    //console.log("Body: ", body);
    return this.httpClient.post(`${this.baseUrl}api/users/`, body, {headers: this.getAuthHeaders()});
  }

  getAuthHeaders() {
    const token = this.cookieService.get('patient-token');
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Token ${token}`
    });
  }
}
