import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  user = '';


  @Output()
  registrationOk = new EventEmitter<boolean>();

  // @ts-ignore
  username: string;
  // @ts-ignore
  password: string;
  // @ts-ignore


  constructor(
    private apiService: ApiService,
  ) { }

  ngOnInit(): void {
  }

  registerOp() {
    this.apiService.createUser(this.username, this.password).subscribe(
      () => {
        alert("Nuovo Operatore Aggiunto con Successo!")
        this.registrationOk.emit(true)
      }

    )

  }

}
